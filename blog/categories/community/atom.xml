<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">

  <title><![CDATA[Category: Community | Home Assistant]]></title>
  <link href="https://home-assistant.io/blog/categories/community/atom.xml" rel="self"/>
  <link href="https://home-assistant.io/"/>
  <updated>2017-09-02T20:55:31+00:00</updated>
  <id>https://home-assistant.io/</id>
  <author>
    <name><![CDATA[Home Assistant]]></name>
    
  </author>
  <generator uri="http://octopress.org/">Octopress</generator>

  
  <entry>
    <title type="html"><![CDATA[Home Assistant is moving to Discord]]></title>
    <link href="https://home-assistant.io/blog/2017/07/03/home-assistant-is-moving-to-discord/"/>
    <updated>2017-07-03T23:28:01+00:00</updated>
    <id>https://home-assistant.io/blog/2017/07/03/home-assistant-is-moving-to-discord</id>
    <content type="html"><![CDATA[Communities grow, things change. We understand that some people don't like change, and that is why we are trying to make our chat transition from Gitter to [Discord][discord] as smooth as possible for everyone. Join us now with [just a click][discord]!

Click `Read on →` to find out more about why we're moving.

<!--more-->

### <a class='title-link' name='easy-to-join' href='#easy-to-join'></a> Easy to join 

Discord is a very easy platform to join. New users can immediately jump into the conversations without first having to create an account. Just provide your name and you're in!

This is incredibly valuable for us and will make Home Assistant even more accessible than before.

### <a class='title-link' name='great-apps' href='#great-apps'></a> Great apps 

One of our least favorite things about Gitter is the quality of apps that they provide for iOS and Android devices. Discord, on the other hand, has the fastest and most feature-rich mobile apps that we've tried! For those of you who like to post pictures to the chatrooms directly from your phone...now you can. It doesn't stop there; they also provide well-polished clients for Windows, macOS and even Linux. If you're more of a browser-based chat client person, their web client will be perfect for you.

### <a class='title-link' name='moderation-tools' href='#moderation-tools'></a> Moderation tools 

We have been quite lucky so far as to not have much inappropriate behavior (spammers, trolls, etc) in our chatrooms. However, as our community continues to grow, this common issue might come up. Discord has the features that will help us shut this behavior down before it gets out of hand.

### <a class='title-link' name='great-multiple-server-support' href='#great-multiple-server-support'></a> Great multiple server support 

Do you already have a Discord account? Great! You can use that account to [join in on the conversation][discord] now. One Discord account works with every Discord server. It is extremely easy to switch between servers or join new ones. Discord messages also work across servers, so your personal conversations are not scoped to a single server.

### <a class='title-link' name='its-hosted' href='#its-hosted'></a> It's hosted 

We are big fans of self-hosted apps and services, but everyone knows they require a fair amount of maintenance and attention to keep them running smoothly. Discord is hosted on its own servers, so it's better to let them maintain the service while we keep busy improving Home Assistant to bring you new and exciting features!

### <a class='title-link' name='plenty-of-features' href='#plenty-of-features'></a> Plenty of features 

Syntax highlighting, voice chats, ability to search chatrooms, private messaging, and even custom emoji! Discord has a great set of features that will keep us all happy for a long time to come. (Pssst...after you join our server, check out the `:ponder:` emoji!)

## <a class='title-link' name='join-us' href='#join-us'></a> Join us! 

Are you convinced and ready to make the switch? Join us, we're just [one click away][discord]!

Not convinced? Please take a moment to reach out to us in the comments below to provide your feedback on this change so that we can help make the switch easier for you and everyone else.

## <a class='title-link' name='the-fine-print' href='#the-fine-print'></a> The fine print... 

In the process of switching chat platforms we have decided to create what we feel is a very fair set of rules for our chat community. As growth continues, these rules might change. Also be sure to check the [#welcome-rules][discord] channel for the most up-to-date rules for our chat server.

### <a class='title-link' name='the-rules' href='#the-rules'></a> The rules... 

#### <a class='title-link' name='rules' href='#rules'></a> Rules: 

Please carefully read through these rules **before engaging in conversation.**

1. **New members: Welcome!** Start by reading over the [FAQ]. Feel free to introduce yourself, as we are all friends here!

2. If you have a question, please check the [FAQ] and [relevant documentation][docs] before posting.

3. Do not insult, belittle, or abuse your fellow community members. Any reports of abuse will not be taken lightly and **will lead to a ban.**

4. Our moderators are kind enough to volunteer their time to help keep this a great community for everyone. Any inappropriate or unconstructive comments toward or about them **will result in a ban.**

5. [#devs][discord-devs] is for **development discussion only**. [#general][discord-general] is for common discussion, support questions, and lending help to others. Please use the appropriate channel as it pertains to the nature of your discussion.

6. Spam will not be tolerated, including but not limited to: self-promotion, flooding, codewalls (longer than 15 lines) and unapproved bots.

These rules are not to be interpreted how you like, there are no "loopholes." Anyone claiming not to be breaking the rules due to it "not being in the rules" will result in the according consequence. If you are unsure about something, please ask either myself (@dale3h) or the community.

If you have any issues with anything or anyone on the server please PM me (@dale3h) with any relevant details. **I cannot help anyone if I am unaware of any issues.**

#### <a class='title-link' name='infractions-and-bans' href='#infractions-and-bans'></a> Infractions and Bans: 

Bans **will be issued** after one serious infraction or failing to acknowledge warnings of minor infractions. This is non-negotiable.

Sincerely,

**Dale Higgs**<br>
Community Leader, Home Assistant

[discord]: https://discord.gg/c5DvZ4e
[discord-devs]: https://discord.gg/8X8DTH4
[discord-general]: https://discord.gg/pywKZRT
[FAQ]: https://home-assistant.io/faq/
[docs]: https://home-assistant.io/docs/
]]></content>
  </entry>
  
  <entry>
    <title type="html"><![CDATA[Home Assistant at PyCon US 2017]]></title>
    <link href="https://home-assistant.io/blog/2017/05/19/home-assistant-at-pycon-us-2017/"/>
    <updated>2017-05-19T00:00:01+00:00</updated>
    <id>https://home-assistant.io/blog/2017/05/19/home-assistant-at-pycon-us-2017</id>
    <content type="html"><![CDATA[In just 12 hours PyCon US 2017 starts. This is an exciting conference because there will be a bunch of Home Assistant developers and users there being able to meet in person.

Just like last year, we'll be hosting a [Home Assistant Open Space](https://us.pycon.org/2017/events/open-spaces/). Please stop by to ask any questions you have to the available developers. ~~We haven't reserved a slot yet but will do so tomorrow. We'll update this post and announce it on social media as we know time and location.~~ **First open-space will be Friday at 4pm in room B112.**

We will also be taken part in the [development sprints](https://us.pycon.org/2017/community/sprints/) from Monday-Wednesday. You only need to bring a laptop. We'll help you get set up with a dev environment and you can be coding on top of Home Assistant in no time. ~~Exact location inside the conference center to be announced.~~ **We are sitting in room A108.**

On a final note, the Home Assistant community is very very active so don't take it personal if we don't remember your name, issue or contribution.
]]></content>
  </entry>
  
  <entry>
    <title type="html"><![CDATA[Grazer Linuxtage 2017: Python Everywhere]]></title>
    <link href="https://home-assistant.io/blog/2017/05/07/grazer-linuxtage-2017-talk-python-everywhere/"/>
    <updated>2017-05-07T02:00:00+00:00</updated>
    <id>https://home-assistant.io/blog/2017/05/07/grazer-linuxtage-2017-talk-python-everywhere</id>
    <content type="html"><![CDATA[I like Python. It's a clean easy to read, easy to learn language. Yet when you use it for some time you still find more features to improve your coding. What I probably like most about Python is the community and the great libraries that already exist. Often solving a problem means including a pre-existing library and writing some glue code. That makes it quick to get things up and running.

I just gave a talk on how you can run Python to automate your home (yes with Home-Assistant) but also with [MicroPython]. Micropython allows you to run Python on your DIY sensors and switches around your home. Python everywhere - even on the chips that give Home-Assistant the data to be awesome.

<div class='videoWrapper'>
<iframe width="560" height="315" src="https://www.youtube.com/embed/KNFZSSCPUyM" frameborder="0" allowfullscreen></iframe>
</div>

[MicroPython]: https://micropython.org
]]></content>
  </entry>
  
  <entry>
    <title type="html"><![CDATA[Hardware Contest 2017]]></title>
    <link href="https://home-assistant.io/blog/2017/04/24/hardware-contest-2017/"/>
    <updated>2017-04-24T06:00:00+00:00</updated>
    <id>https://home-assistant.io/blog/2017/04/24/hardware-contest-2017</id>
    <content type="html"><![CDATA[We have four submissions for our [Hardware Contest 2017][hardware].

- [Part of IoT-course](https://community.home-assistant.io/t/entry-for-hardware-contest-part-of-iot-course/14827)
- [Automating a nursing home!](https://community.home-assistant.io/t/entry-automating-a-nursing-home/14872)
- [Dedicated hardware for coding, testing, building, and contributing to Home Assistant](https://community.home-assistant.io/t/entry-dedicated-hardware-for-coding-testing-building-and-contributing-to-home-assistant/15515)
- [HassIO - Home Assistant hub for dummies](https://community.home-assistant.io/t/entry-hassio-home-assistant-hub-for-dummies/16037)

The voting is now open. To keep things simple are we using the voting feature of the [forum]. Vote for your choices.

End of the voting period: April, 30 2017 - 23.59 UTC

[hardware]: https://home-assistant.io/blog/2017/04/01/thomas-krenn-award/
[award]: https://www.thomas-krenn.com/de/tkmag/allgemein/zammad-home-assistant-und-freifunk-das-sind-die-gewinner-des-thomas-krenn-awards-2017/
[forum]: https://community.home-assistant.io/c/contest-2017
[twitter]: https://twitter.com/home_assistant

]]></content>
  </entry>
  
  <entry>
    <title type="html"><![CDATA[Thomas Krenn award 2017]]></title>
    <link href="https://home-assistant.io/blog/2017/04/01/thomas-krenn-award/"/>
    <updated>2017-04-01T06:00:00+00:00</updated>
    <id>https://home-assistant.io/blog/2017/04/01/thomas-krenn-award</id>
    <content type="html"><![CDATA[You may already know from our social media channels and the release blog post for 0.41: We are now an award-winning Open source project. The jury of the [Thomas-Krenn-Award][award] put us on the 2nd place. This is an awesome achievment for an independent community project.

I would like to thanks all contributors. Your endless effort made this possible.

<img src='/images/blog/2017-04-award/award.jpg' style='border: 0;box-shadow: none;'>

The prize beside the very nice trophy contains hardware and we want to give that hardware partically away. We won four [Low Energy Server v2 (LES)][LES] units with an Intel Celeron N2930, 8 GB of RAM, and a mSATA of 128 GB (one unit with 64 GB). We were thinking about to keep one of those units in Europe and one in North America for testing and to use during workshops and events. But the other two will go to interested parties.

As a raffle would be to easy, we make a contest out of it. This means that we are looking for your application. Of course, we would like to see those systems goes to active or future developers who can justify their need for one of the systems to run CI, UI tests, public accessible Home Assistant demo instances, etc. At the other hand we would like to keep it open, to see with what people are coming up. Please participate as well if you are planning to automate the public school of your kids with 1000 switches or light, need a server to run it, and want to provide regular feedback about the user experience.

Create an entry in our [Forum][forum]. Be creative and use your imagination.


### <a class='title-link' name='the-details' href='#the-details'></a> The details 

- Jury: The Home Assistant community
- Dead line: April, 23 2017 - 23.59 UTC
- Voting period: April, 24th till April, 30 2017 - 23.59 UTC

The decision of the jury will be final. If there will be a dispute then the Top-5 commiter of the Home Assistant organisation on Github will decide. Also, we reserve us the right to ban applications if we suspect cheating or unfair methods. Updates will be available in the [Forum][forum] and on [Twitter][twitter].

Keep in mind that you may have to pay the fee for customs handling and the import duty by yourself. The plan is to ship the hardware from Germany. If you are located in a country with import/export regulations, we may not be able to ship the hardware to you.

[LES]: https://www.thomas-krenn.com/en/products/low-energy-systems/les-v2.html
[award]: https://www.thomas-krenn.com/de/tkmag/allgemein/zammad-home-assistant-und-freifunk-das-sind-die-gewinner-des-thomas-krenn-awards-2017/
[forum]: https://community.home-assistant.io/c/contest-2017
[twitter]: https://twitter.com/home_assistant

]]></content>
  </entry>
  
</feed>
